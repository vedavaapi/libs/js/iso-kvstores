export interface IStoreOptions {
    name: string;
    databaseName?: string;
    maxHeapSize?: number;
}
