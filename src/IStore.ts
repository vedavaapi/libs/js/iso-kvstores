export interface IStore {
    readonly maxHeapSize: number;
    readonly name: string;
    readonly type: string;
    clear(): Promise<void>;
    delete(key: string): Promise<boolean>;
    entries(keys?: string[]): Promise<[string, any][]>;
    get(key: string): Promise<any>;
    has(key: string): Promise<boolean>;
    import(entries: [string, any][]): Promise<void>;
    set(key: string, value: any): Promise<void>;
    size(): Promise<number>;
    keys(): Promise<string[]>;
}
