/* eslint-disable no-underscore-dangle */

/**
 * adapted from https://github.com/badbatch/cachemap/blob/master/packages/redis/src/main/index.ts
 */

import { RedisClient } from 'redis';

import { IStore } from './IStore';
import { IStoreInit } from './IStoreInit';
import { IStoreOptions } from './IStoreOptions';


export interface IConstructorOptions {
    client: RedisClient;
    maxHeapSize?: number;
    name: string;
}

export interface InitOptions extends IOptions, IStoreOptions {
    name: string;
    databaseName?: string;
}

export interface IOptions {
    maxHeapSize?: number;
    client: RedisClient;
}


export class RedisStore implements IStore {
    public static async init(options: InitOptions): Promise<RedisStore> {
        const { maxHeapSize, name, client } = options;

        try {
            return new RedisStore({ client, maxHeapSize, name });
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public readonly type = 'redis';

    private _client: RedisClient;

    private _maxHeapSize: number = Infinity;

    private _name: string;

    constructor(options: IConstructorOptions) {
        this._client = options.client;

        if (options.maxHeapSize) {
            this._maxHeapSize = options.maxHeapSize;
        }

        this._name = options.name;
    }

    get maxHeapSize() {
        return this._maxHeapSize;
    }

    get name() {
        return this._name;
    }

    public async clear(): Promise<void> {
        return new Promise((resolve: (value: undefined) => void, reject: (reason: Error) => void) => {
            this._client.flushdb((error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(undefined);
                }
            });
        });
    }

    public async delete(key: string): Promise<boolean> {
        return new Promise((resolve: (value: boolean) => void, reject: (reason: Error) => void) => {
            this._client.del(key, (error, reply) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(!!reply);
                }
            });
        });
    }

    public async entries(keys?: string[]): Promise<[string, any][]> {
        if (!keys) {
            // eslint-disable-next-line no-param-reassign
            keys = await this.keys();
        }
        if (!(keys as string[]).length) return [];

        const _keys = keys as string[];
        // console.log({ keys, _keys });

        return new Promise((resolve: (value: [string, any][]) => void, reject: (reason: Error) => void) => {
            this._client.mget(_keys, (error, reply) => {
                if (error) {
                    reject(error);
                } else {
                    const entries: [string, any][] = [];

                    _keys.forEach((key, index) => {
                        entries.push([key, JSON.parse(reply[index])]);
                    });

                    resolve(entries);
                }
            });
        });
    }

    public async get(key: string): Promise<any> {
        return new Promise((resolve: (value: any) => void, reject: (reason: Error) => void) => {
            this._client.get(key, (error, reply) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(reply));
                }
            });
        });
    }

    public async has(key: string): Promise<boolean> {
        return new Promise((resolve: (value: boolean) => void, reject: (reason: Error) => void) => {
            this._client.exists(key, (error, reply) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(!!reply);
                }
            });
        });
    }

    public async import(entries: [string, any][]): Promise<void> {
        const _entries: string[] = [];

        entries.forEach(([key, value]) => {
            _entries.push(key, JSON.stringify(value));
        });

        return new Promise((resolve: (value: undefined) => void, reject: (reason: Error) => void) => {
            this._client.mset(_entries, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(undefined);
                }
            });
        });
    }

    public async set(key: string, value: any): Promise<void> {
        return new Promise((resolve: (value: undefined) => void, reject: (reason: Error) => void) => {
            this._client.set(key, JSON.stringify(value), (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(undefined);
                }
            });
        });
    }

    public async size(): Promise<number> {
        return new Promise((resolve: (value: number) => void, reject: (reason: Error) => void) => {
            this._client.dbsize((error, reply) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(reply);
                }
            });
        });
    }

    public async keys(): Promise<string[]> {
        const scanAsync = (cursor: string) => new Promise((resolve: (value: [string, string[]]) => void, reject) => {
            this._client.scan(cursor, (e, r) => {
                if (e) {
                    reject(e);
                } else {
                    resolve(r);
                }
            });
        });
        let keys: string[] = [];
        let iterKeys = [];
        let nextCursor = '0';
        let iterComplete = false;
        while (!iterComplete) {
            // eslint-disable-next-line no-await-in-loop
            ([nextCursor, iterKeys] = await scanAsync(nextCursor));
            keys = keys.concat(iterKeys);
            if (nextCursor === '0') {
                iterComplete = true;
            }
        }
        return keys;
    }
}

export default function init(options: IOptions): IStoreInit {
    return (storeOptions: IStoreOptions) => RedisStore.init({ ...options, ...storeOptions });
}
