/* eslint-disable no-underscore-dangle */

/**
 * adapted from https://github.com/badbatch/cachemap/blob/master/packages/indexed-db/src/main/index.ts
 */

import { IDBPDatabase, openDB } from 'idb';

import { IStore } from './IStore';
import { IStoreInit } from './IStoreInit';
import { IStoreOptions } from './IStoreOptions';


export interface IConstructorOptions extends InitOptions {
    indexedDB: IDBPDatabase;
}

export interface IOptions {
    maxHeapSize?: number;
}

export interface InitOptions extends IOptions, IStoreOptions {
    name: string;
    databaseName?: string;
}


export class IDBStore implements IStore {
    public static async init(options: InitOptions): Promise<IDBStore> {
        try {
            const databaseName = options.databaseName || `${options.name}-store`;
            const objectStoreName = options.name;

            const indexedDB = await openDB(databaseName, 1, {
                upgrade(upgradeDb: IDBPDatabase) {
                    if (!upgradeDb.objectStoreNames.contains(objectStoreName)) {
                        upgradeDb.createObjectStore(objectStoreName);
                    }
                },
            });

            return new IDBStore({
                indexedDB,
                ...options,
            });
        } catch (error) {
            // console.log({ error });
            return Promise.reject(error);
        }
    }

    public readonly type = 'indexedDB';

    private _indexedDB: IDBPDatabase;

    private _maxHeapSize: number = 4194304;

    private _name: string;

    constructor(options: IConstructorOptions) {
        this._indexedDB = options.indexedDB;

        if (options.maxHeapSize) {
            this._maxHeapSize = options.maxHeapSize;
        }

        this._name = options.name;
    }

    get maxHeapSize() {
        return this._maxHeapSize;
    }

    get name() {
        return this._name;
    }

    public async clear(): Promise<void> {
        const tx = this._indexedDB.transaction(this._name, 'readwrite');
        await tx.objectStore(this._name).clear();
        await tx.done;
    }

    public async delete(key: string): Promise<boolean> {
        try {
            if ((await this.get(key)) === undefined) return false;
            const tx = this._indexedDB.transaction(this._name, 'readwrite');
            await tx.objectStore(this._name).delete(key);
            await tx.done;
            return true;
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public async entries(keys?: string[]): Promise<[string, any][]> {
        try {
            const tx = this._indexedDB.transaction(this._name);
            const entries: [string, any][] = [];
            let cursor = await tx.objectStore(this._name).openCursor();

            while (cursor) {
                const key = cursor.key as string;

                if (keys) {
                    if (keys.includes(key)) entries.push([key, cursor.value]);
                } else entries.push([key, cursor.value]);

                // eslint-disable-next-line no-await-in-loop
                cursor = await cursor.continue(); // as it is cursor, first it has to complete before next starts.
            }

            await tx.done;
            return entries;
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public async get(key: string): Promise<any> {
        try {
            const tx = this._indexedDB.transaction(this._name);
            return tx.objectStore(this._name).get(key);
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public async has(key: string): Promise<boolean> {
        try {
            const entry = await this.get(key);
            return entry !== undefined;
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public async import(entries: [string, any][]): Promise<void> {
        const tx = this._indexedDB.transaction(this._name, 'readwrite');
        await Promise.all(entries.map(([key, value]) => tx.objectStore(this._name).put(value, key)));
        await tx.done;
    }

    public async set(key: string, value: any): Promise<void> {
        const tx = this._indexedDB.transaction(this._name, 'readwrite');
        await tx.objectStore(this._name).put(value, key);
        await tx.done;
    }

    public async size(): Promise<number> {
        return (await this.keys()).length;
    }

    public async keys(): Promise<string[]> {
        const tx = this._indexedDB.transaction(this._name);
        const keys: (IDBKeyRange | IDBValidKey)[] = [];
        let cursor = await tx.objectStore(this._name).openCursor();

        while (cursor) {
            keys.push(cursor.key);
            // eslint-disable-next-line no-await-in-loop
            cursor = await cursor.continue();
        }

        await tx.done;
        return keys as string[];
    }
}

export default function init(options: IOptions = {}): IStoreInit {
    return (storeOptions: IStoreOptions) => IDBStore.init({ ...options, ...storeOptions });
}
