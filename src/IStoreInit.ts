import { IStoreOptions } from './IStoreOptions';
import { IStore } from './IStore';

export type IStoreInit = (options: IStoreOptions) => Promise<IStore>;
